﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{ [Tooltip("Daño que causa la bala en los enemigos")]
    public int damage;
    
    
 

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<EnemyController>().life -= damage;
        }
        Destroy(gameObject);

    }
   
}
