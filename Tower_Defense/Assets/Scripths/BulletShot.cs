﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float timelastShot;

    public float shotfrequency;
    public List<Transform> enemiesToshot;
    // Start is called before the first frame update
    void Start()
    {
        enemiesToshot = new List<Transform>();
      
    }

    // Update is called once per frame
    void Update()
    {
        if((enemiesToshot.Count > 0) && (enemiesToshot[0] == null))

        {
            enemiesToshot.RemoveAt(0);
        }
        
         if(enemiesToshot.Count > 0)
        {
            if(Time.time -timelastShot >shotfrequency)
            {
                GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
                bullet.GetComponent<BulletMovement>().enemy = enemiesToshot[0];
                timelastShot = Time.time;
            }
        }
        
    }



}
