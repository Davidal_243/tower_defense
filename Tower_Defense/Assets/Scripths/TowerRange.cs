﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerRange : MonoBehaviour
{
    
 
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Enemy"))
        {

            GetComponent<BulletShot>().enemiesToshot.Add(other.transform);
          
        }

    }

    private void OnTriggerExit(Collider other)
    {
        
        if (other.gameObject.CompareTag("Enemy"))
        {

            GetComponent<BulletShot>().enemiesToshot.Remove(other.transform);

        }
    }





}
