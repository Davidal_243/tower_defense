﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public int Life = 20;

    public int currentHealth;
    public HealtBar healtBar;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = Life;
        healtBar.SetMaxHealth(Life);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            currentHealth -= 5;
            healtBar.SetHealth(currentHealth);
            if(currentHealth < 0)
                {
                Destroy(gameObject);
                }

        }
    }
}
