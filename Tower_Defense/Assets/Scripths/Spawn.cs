﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float timeRegeneration;
    public float spawnTime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - timeRegeneration > spawnTime)
        {
            Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            timeRegeneration = Time.time;
        }
    }
}
