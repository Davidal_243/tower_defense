﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public Vector3 targetPosition;
    public  int life;
    public int currentHealth;
    public HealtBar healtBar;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<NavMeshAgent>().SetDestination(targetPosition);
        currentHealth = life;
        healtBar.SetMaxHealth(life);
    }

    // Update is called once per frame
    void Update()
    {
        healtBar.SetHealth(currentHealth);
        if (currentHealth < 0)
        {
            Destroy(gameObject);
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Tower"))
            { Destroy(gameObject); }
    }


}
