﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float bulletSpeed;

    public Transform enemy;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 8f);
    }

    // Update is called once per frame
    void Update()
    {
        if (enemy != null)
        {
            //transform.Translate(0, bulletSpeed, 0);
            transform.position = Vector3.MoveTowards(transform.position, enemy.position, bulletSpeed);

        }else
        {
            Destroy(gameObject);
        }
    }


   

}
